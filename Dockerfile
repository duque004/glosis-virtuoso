FROM tenforce/virtuoso:latest

ENV DBA_PASSWORD toto 
ENV SPARQL_UPDATE true
ENV DEFAULT_GRAPH http://www.example.com/my-graph
 
################### Usage #####################

# Build it
# docker build --force-rm -t glosis/virtuoso . -f ./Dockerfile 

# Run command
# docker run --name glosis-virtuoso \
#    -p 8890:8890 -p 1111:1111 \
#    -v /home/duque004/git/glosis-virtuoso/data:/data \
#    -d glosis/virtuoso

